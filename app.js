function updateProductNumber(product, price, isIncreasing) {
    const productInput = document.getElementById(product + '-number');
    let productNumber = productInput.value;
    if (isIncreasing == true) {
        productNumber = parseInt(productNumber) + 1;
    }
    else if (isIncreasing == false && productNumber > 0) {
        productNumber = parseInt(productNumber) - 1;
    }
    productInput.value = productNumber;
    // maj produit total
    const productTotal = document.getElementById(product + '-total');
    productTotal.innerText = productNumber * price;
    // calculer total
    calculateTotal();
}

function getInputValue(product) {
    const productInput = document.getElementById(product + '-number');
    const productNumber = parseInt(productInput.value);
    return productNumber;
}
function calculateTotal() {
    const phoneTotal = getInputValue('phone') * 150000;
    const caseTotal = getInputValue('headset') * 130000;
    const subTotal = phoneTotal + caseTotal;
    const tax = subTotal / 10;
    const totalPrice = subTotal + tax;
    // maj html
    document.getElementById('sub-total').innerText = subTotal;
    document.getElementById('tax-amount').innerText = tax;
    document.getElementById('total-price').innerText = totalPrice;

}
function removeProduct(item) {
    document.getElementById(item + '-display').style.display = 'none';
}


//incrementation phone
document.getElementById("phone-plus").addEventListener('click', function () {
    updateProductNumber('phone', 150000, true);
});
document.getElementById("phone-minus").addEventListener('click', function () {
    updateProductNumber('phone', 150000, false);
});

// incrementation headset
document.getElementById("headset-plus").addEventListener('click', function () {
    updateProductNumber('headset', 130000, true);
});
document.getElementById("headset-minus").addEventListener('click', function () {
    updateProductNumber('headset', 130000, false);
});

// effacer
document.getElementById("phone-remove").addEventListener('click', function () {
    removeProduct('phone');
});
document.getElementById("headset-remove").addEventListener('click', function () {
    removeProduct('headset');
});

// verifier
document.getElementById("check-btn").addEventListener('click', function () {
    alert('Succès');
});

const cart = document.querySelector(".heart"),
favorite = cart.querySelector(".like")

favorite.addEventListener("click", ()=> cart.classList.toggle("liked"));